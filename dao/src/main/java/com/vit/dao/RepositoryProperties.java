package com.vit.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by evtushenkovv on 01.07.15.
 */
public class RepositoryProperties {

    private String repositoryType;
    private String productRepositoryPath;
    private String categoryRepositoryPath;

    private String driverClassName;
    private String url;
    private String username;
    private String password;

    private static class PropertiesHolder {
        private static RepositoryProperties instance = new RepositoryProperties();
     }

    private RepositoryProperties() {

        Properties properties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try (InputStream input = loader.getResourceAsStream("repository.properties")) {
            properties.load(input);
            this.repositoryType = properties.getProperty("repository.type", "COLLECTION");

            if (repositoryType.equals("JDBC")) {
                this.productRepositoryPath = properties.getProperty("repository.file.path", "")
                        + properties.getProperty("repository.file.name.product", "product")
                        + properties.getProperty("repository.file.extension", "");
                this.categoryRepositoryPath = properties.getProperty("repository.file.path", "")
                        + properties.getProperty("repository.file.name.category", "category")
                        + properties.getProperty("repository.file.extension", "");
                String filesPathName = properties.getProperty("repository.file.path", "");
                if (!filesPathName.isEmpty()) {
                    Path filesPath = Paths.get(filesPathName);
                    if (Files.notExists(filesPath)) {
                        Files.createDirectory(filesPath);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if (repositoryType.equals("JDBC")) {
            try (InputStream input = loader.getResourceAsStream("dbconnection.properties")) {
                properties.load(input);
                driverClassName = properties.getProperty("jdbc.name");
                url = properties.getProperty("jdbc.url");
                username = properties.getProperty("jdbc.username");
                password = properties.getProperty("jdbc.password", "");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static RepositoryProperties getInstance() {
        return PropertiesHolder.instance;
    }

    public String getRepositoryType() {
        return repositoryType;
    }

    public String getProductRepositoryPath() {
        return productRepositoryPath;
    }

    public String getCategoryRepositoryPath() {
        return categoryRepositoryPath;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
