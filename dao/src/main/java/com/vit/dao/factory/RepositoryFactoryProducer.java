package com.vit.dao.factory;

import com.vit.dao.factory.impl.*;

/**
 * Created by evtushenkovv on 01.07.15.
 */
public class RepositoryFactoryProducer {

    public static RepositoryFactory getFactory(String repositoryType) {

        if (repositoryType.equalsIgnoreCase("COLLECTION")) {
            return new CollectionRepositoryFactory();
        } else  if (repositoryType.equalsIgnoreCase("SERIALIZATION")) {
            return new SerializationRepositoryFactory();
        } else if (repositoryType.equalsIgnoreCase("XML")) {
            return new XmlRepositoryFactory();
        } else if (repositoryType.equalsIgnoreCase("JSON")) {
            return new JsonRepositoryFactory();
        } else if (repositoryType.equalsIgnoreCase("JDBC")) {
            return new JdbcRepositoryFactory();
        }

        return null;
    }
}
