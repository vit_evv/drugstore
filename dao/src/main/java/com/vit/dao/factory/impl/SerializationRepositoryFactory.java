package com.vit.dao.factory.impl;

import com.vit.dao.CategoryRepository;
import com.vit.dao.ProductRepository;
import com.vit.dao.factory.RepositoryFactory;
import com.vit.dao.impl.serialization.CategoryRepositorySerializationImpl;
import com.vit.dao.impl.serialization.ProductRepositorySerializationImpl;

/**
 * Created by evtushenkovv on 01.07.15.
 */
public class SerializationRepositoryFactory implements RepositoryFactory {

    @Override
    public ProductRepository getProductRepository() {
        return new ProductRepositorySerializationImpl();
    }

    @Override
    public CategoryRepository getCategoryRepository() {
        return new CategoryRepositorySerializationImpl();
    }
}
