package com.vit.dao.factory.impl;

import com.vit.dao.CategoryRepository;
import com.vit.dao.ProductRepository;
import com.vit.dao.factory.RepositoryFactory;
import com.vit.dao.impl.collections.CategoryRepositoryCollectionImpl;
import com.vit.dao.impl.collections.ProductRepositoryCollectionImpl;
import com.vit.dao.impl.jdbc.CategoryRepositoryJdbcImpl;
import com.vit.dao.impl.jdbc.ProductRepositoryJdbcImpl;

/**
 * Created by evtushenkovv on 01.07.15.
 */
public class JdbcRepositoryFactory implements RepositoryFactory {

    @Override
    public ProductRepository getProductRepository() {
        return new ProductRepositoryJdbcImpl();
    }

    @Override
    public CategoryRepository getCategoryRepository() {
        return new CategoryRepositoryJdbcImpl();
    }
}
