package com.vit.dao.factory.impl;

import com.vit.dao.CategoryRepository;
import com.vit.dao.ProductRepository;
import com.vit.dao.factory.RepositoryFactory;
import com.vit.dao.impl.xml.CategoryRepositoryXmlImpl;
import com.vit.dao.impl.xml.ProductRepositoryXmlImpl;

/**
 * Created by evtushenkovv on 01.07.15.
 */
public class XmlRepositoryFactory implements RepositoryFactory {

    @Override
    public ProductRepository getProductRepository() {
        return new ProductRepositoryXmlImpl();
    }

    @Override
    public CategoryRepository getCategoryRepository() {
        return new CategoryRepositoryXmlImpl();
    }
}
