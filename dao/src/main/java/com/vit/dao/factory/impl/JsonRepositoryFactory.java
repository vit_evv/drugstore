package com.vit.dao.factory.impl;

import com.vit.dao.CategoryRepository;
import com.vit.dao.ProductRepository;
import com.vit.dao.factory.RepositoryFactory;
import com.vit.dao.impl.json.CategoryRepositoryJsonImpl;
import com.vit.dao.impl.json.ProductRepositoryJsonImpl;

/**
 * Created by evtushenkovv on 01.07.15.
 */
public class JsonRepositoryFactory implements RepositoryFactory {

    @Override
    public ProductRepository getProductRepository() {
        return new ProductRepositoryJsonImpl();
    }

    @Override
    public CategoryRepository getCategoryRepository() {
        return new CategoryRepositoryJsonImpl();
    }
}
