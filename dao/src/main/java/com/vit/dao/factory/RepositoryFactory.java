package com.vit.dao.factory;

import com.vit.dao.CategoryRepository;
import com.vit.dao.ProductRepository;

/**
 * Created by evtushenkovv on 01.07.15.
 */
public interface RepositoryFactory {

    ProductRepository getProductRepository();

    CategoryRepository getCategoryRepository();

}
