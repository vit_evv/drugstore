package com.vit.dao.impl.serialization;

import com.vit.dao.CategoryRepository;
import com.vit.dao.RepositoryProperties;
import com.vit.model.Category;
import com.vit.model.Product;

import java.io.*;
import java.util.*;

/**
 * Created by evtushenkovv on 24.06.15.
 */
public class CategoryRepositorySerializationImpl implements CategoryRepository{

    private static Long idCount = 0L;
    private static Map<Long, Category> repository = new HashMap<>();
    private static String fileName
            = RepositoryProperties.getInstance().getCategoryRepositoryPath();

    static {
        readFromFile();
        idCount = (repository.size() == 0) ? 0 : repository.size()-1L;
    }

    @Override
    public Long create(Category object) {
        object.setId(idCount++);
        repository.put(object.getId(), object);
        saveToFile();
        return object.getId();
    }

    @Override
    public void update(Category object) {
        repository.put(object.getId(),object);
        saveToFile();
    }

    @Override
    public void delete(Category object) {
        repository.remove(object.getId());
        saveToFile();
    }

    @Override
    public Category getById(Long id) {
        return repository.get(id);
    }

    @Override
    public List<Category> getAll() {
        return new ArrayList<Category>(repository.values());
    }

    @Override
    public List<Category> getAllSortedByAvgProductsPrice() {

        final List<Category> categories = new ArrayList<>(repository.values());

        Collections.sort(categories, new Comparator<Category>() {
            public int compare(Category category1, Category category2) {

                //descending order
                return Double.compare(getAvgPriceByCategory(category1), getAvgPriceByCategory(category2));
            }
        });

        return categories;
    }

    private Double getAvgPriceByCategory(Category category) {

        if (category.getProducts() == null) {
            return 0.0;
        }
        if (category.getProducts().size() == 0) {
            return 0.0;
        }

        Double sumPricesByCategory = 0.0;
        for(Product product: category.getProducts()) {
            sumPricesByCategory += (product.getPrice() == null)
                    ? 0 : product.getPrice();
        }

        return sumPricesByCategory/category.getProducts().size();
    }

    private static void saveToFile() {
        try (ObjectOutputStream stream =
                     new ObjectOutputStream(new FileOutputStream(fileName))) {
            stream.writeObject(repository);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readFromFile() {
        try (ObjectInputStream stream =
                     new ObjectInputStream(new FileInputStream(fileName))) {
            repository = (Map<Long, Category>) stream.readObject();
        }
        catch (ClassNotFoundException e) {
            System.out.println("Something wrong with objects in your file. Check the file content!");
            e.printStackTrace();
        }
        catch (FileNotFoundException e) {
            System.out.println("File with name ("+fileName+") was not found. Check the file!");
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
