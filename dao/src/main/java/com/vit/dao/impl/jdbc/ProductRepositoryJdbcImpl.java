package com.vit.dao.impl.jdbc;

import com.vit.dao.ProductRepository;
import com.vit.dao.RepositoryProperties;
import com.vit.model.Product;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by evtushenkovv on 08.07.15.
 */
@Repository("productRepositoryJdbc")
public class ProductRepositoryJdbcImpl implements ProductRepository {

    private static RepositoryProperties repositoryProperties = RepositoryProperties.getInstance();

    static {
        try {
            Class.forName(repositoryProperties.getDriverClassName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void truncate() {

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             Statement ps = connection.createStatement()) {

            ps.execute("DROP TABLE IF EXISTS product");
            String sql = "CREATE TABLE IF NOT EXISTS `product` ( " +
                    "  `id` BIGINT NOT NULL AUTO_INCREMENT, " +
                    "  `name` VARCHAR(50) NOT NULL, " +
                    "  `description` TEXT NULL, " +
                    "  `sku` VARCHAR(16) NOT NULL, " +
                    "  `price` DECIMAL(15,3) ZEROFILL NULL DEFAULT 0.000, " +
                    "  `expiration_date` TIMESTAMP NULL, " +
                    "  `category_id` BIGINT NULL, " +
                    "  PRIMARY KEY (`id`), " +
                    "  UNIQUE INDEX `id_UNIQUE` (`id` ASC), " +
                    "  UNIQUE INDEX `name_UNIQUE` (`name` ASC), " +
                    "  UNIQUE INDEX `sku_UNIQUE` (`sku` ASC), " +
                    "    CONSTRAINT `category_id` " +
                    "    FOREIGN KEY (`category_id`) " +
                    "    REFERENCES `category` (`id`) " +
                    "    ON DELETE SET NULL " +
                    "    ON UPDATE CASCADE) " +
                    "\tENGINE=InnoDB DEFAULT CHARSET=utf8 ";
            ps.execute(sql);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Long create(Product object) {

        String sql = "INSERT INTO product " +
                "(name, description, sku, price, expiration_date, category_id) " +
                "VALUES (?, ?, ?, ?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql
                     , Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, object.getName());
            ps.setString(2, object.getDescription());
            ps.setString(3, object.getSku());
            ps.setDouble(4, object.getPrice());
            ps.setDate(5, object.getExpirationDate());
            if (object.getCategory() == null) {
                ps.setNull(6, Types.NULL);
            } else {
                ps.setLong(6, object.getCategory().getId());
            }

            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next())
            {
                return rs.getLong(1);
            }
            return null;

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
    }

    @Override
    public void update(Product object) {

        String sql = "UPDATE product " +
                "SET name = ? " +
                ", description = ? " +
                ", sku = ? " +
                ", price = ? " +
                ", expiration_date = ? " +
                ", category_id = ? " +
                "WHERE id = ?";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, object.getName());
            ps.setString(2, object.getDescription());
            ps.setString(3, object.getSku());
            ps.setDouble(4, object.getPrice());
            ps.setDate(5, object.getExpirationDate());
            if (object.getCategory() == null) {
                ps.setNull(6, Types.NULL);
            } else {
                ps.setLong(6, object.getCategory().getId());
            }
            ps.setLong(7, object.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }

    }

    @Override
    public void delete(Product object) {

        String sql = "DELETE FROM product WHERE id = ?";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, object.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
    }

    @Override
    public Product getById(Long id) {

        Product product = null;
        String sql = "SELECT id, name, description, sku, price" +
                ", expiration_date, category_id FROM product WHERE id = ?";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                product = new Product(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("sku"),
                        rs.getDouble("price"),
                        rs.getDate("expiration_date"),
                        new CategoryRepositoryJdbcImpl().getById(rs.getLong("category_id"))
                );
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
        return product;
    }

    @Override
    public Product getByName(String name) {

        Product product = null;
        String sql = "SELECT id, name, description, sku, price" +
                ", expiration_date, category_id FROM product WHERE name = ?";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                product = new Product(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("sku"),
                        rs.getDouble("price"),
                        rs.getDate("expiration_date"),
                        new CategoryRepositoryJdbcImpl().getById(rs.getLong("category_id"))
                );
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
        return product;
    }

    @Override
    public List<Product> getAll() {

        Product product = null;
        List<Product> products = new ArrayList<>();
        String sql = "SELECT id, name, description, sku, price" +
                ", expiration_date, category_id FROM product";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                product = new Product(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("sku"),
                        rs.getDouble("price"),
                        rs.getDate("expiration_date"),
                        new CategoryRepositoryJdbcImpl().getById(rs.getLong("category_id"))
                );
                products.add(product);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }

        return products;
    }

    @Override
    public List<Product> getAllSortedByName() {

        Product product = null;
        List<Product> products = new ArrayList<>();
        String sql = "SELECT id, name, description, sku, price" +
                ", expiration_date, category_id FROM product ORDER BY name";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                product = new Product(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("sku"),
                        rs.getDouble("price"),
                        rs.getDate("expiration_date"),
                        new CategoryRepositoryJdbcImpl().getById(rs.getLong("category_id"))
                );
                products.add(product);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }

        return products;
    }

    @Override
    public List<Product> getAllSortedByPrice() {

        Product product = null;
        List<Product> products = new ArrayList<>();
        String sql = "SELECT id, name, description, sku, price" +
                ", expiration_date, category_id FROM product ORDER BY price";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                product = new Product(
                        rs.getLong("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("sku"),
                        rs.getDouble("price"),
                        rs.getDate("expiration_date"),
                        new CategoryRepositoryJdbcImpl().getById(rs.getLong("category_id"))
                );
                products.add(product);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }

        return products;
    }
}
