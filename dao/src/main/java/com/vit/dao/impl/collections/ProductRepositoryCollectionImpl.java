package com.vit.dao.impl.collections;

import com.vit.dao.ProductRepository;
import com.vit.model.Product;

import java.util.*;


/**
 * Created by evtushenkovv on 24.06.15.
 */
public class ProductRepositoryCollectionImpl implements ProductRepository {

    private static Long idCount = 0L;
    private static Map<Long, Product> repository = new HashMap<>();

    public static void truncate() {
        repository.clear();
        idCount = 0L;
    }

    @Override
    public Long create(Product object) {
        object.setId(idCount++);
        repository.put(object.getId(), object);
        return object.getId();
    }

    @Override
    public void update(Product object) {
        repository.put(object.getId(),object);
    }

    @Override
    public void delete(Product object) {
        repository.remove(object.getId());
    }

    @Override
    public Product getById(Long id) {
        return repository.get(id);
    }

    @Override
    public Product getByName(String name) {

        for(Product product: repository.values()) {
            if(product.getName().equals(name)) {
                return product;
            }
        }

        return null;
    }

    @Override
    public List<Product> getAll() {
        return new ArrayList<Product>(repository.values());
    }

    @Override
    public List<Product> getAllSortedByName() {

        List<Product> products = new ArrayList<>(repository.values());

        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product product1, Product product2) {
                String productName1 = product1.getName().toUpperCase();
                String productName2 = product2.getName().toUpperCase();

                //ascending order
                return productName1.compareTo(productName2);
            }
        });

        return products;
    }

    @Override
    public List<Product> getAllSortedByPrice() {
        
        List<Product> products = new ArrayList<>(repository.values());

        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product product1, Product product2) {

                //ascending order
                return Double.compare(product1.getPrice(), product2.getPrice());
            }
        });

        return products;
    }
}
