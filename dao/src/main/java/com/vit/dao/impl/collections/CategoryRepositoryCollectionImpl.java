package com.vit.dao.impl.collections;

import com.vit.dao.CategoryRepository;
import com.vit.model.Category;
import com.vit.model.Product;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by evtushenkovv on 24.06.15.
 */

public class CategoryRepositoryCollectionImpl implements CategoryRepository{

    private static Long idCount = 0L;
    private static Map<Long, Category> repository = new HashMap<>();

    public static void truncate() {
        repository.clear();
        idCount = 0L;
    }

    @Override
    public Long create(Category object) {
        object.setId(idCount++);
        repository.put(object.getId(), object);
        return object.getId();
    }

    @Override
    public void update(Category object) {
        repository.put(object.getId(),object);
    }

    @Override
    public void delete(Category object) {
        repository.remove(object.getId());
    }

    @Override
    public Category getById(Long id) {
        return repository.get(id);
    }

    @Override
    public List<Category> getAll() {
        return new ArrayList<Category>(repository.values());
    }

    @Override
    public List<Category> getAllSortedByAvgProductsPrice() {

        final List<Category> categories = new ArrayList<>(repository.values());

        Collections.sort(categories, new Comparator<Category>() {
            public int compare(Category category1, Category category2) {

                //descending order
                return Double.compare(getAvgPriceByCategory(category1), getAvgPriceByCategory(category2));
            }
        });

        return categories;
    }

    private Double getAvgPriceByCategory(Category category) {

        if (category.getProducts() == null) {
            return 0.0;
        }
        if (category.getProducts().size() == 0) {
            return 0.0;
        }

        Double sumPricesByCategory = 0.0;
        for(Product product: category.getProducts()) {
            sumPricesByCategory += (product.getPrice() == null)
                    ? 0 : product.getPrice();
        }

        return sumPricesByCategory/category.getProducts().size();
    }

}
