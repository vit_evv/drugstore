package com.vit.dao.impl.serialization;

import com.vit.dao.ProductRepository;
import com.vit.dao.RepositoryProperties;
import com.vit.model.Product;

import java.io.*;
import java.util.*;


/**
 * Created by evtushenkovv on 24.06.15.
 */
public class ProductRepositorySerializationImpl implements ProductRepository {

    private static Long idCount = 0L;
    private static Map<Long, Product> repository = new HashMap<>();
    private static String fileName
            = RepositoryProperties.getInstance().getProductRepositoryPath();

    static {
        readFromFile();
        idCount = (repository.size() == 0) ? 0 : repository.size()-1L;
    }

    @Override
    public Long create(Product object) {

        object.setId(idCount++);
        repository.put(object.getId(), object);
        saveToFile();
        return object.getId();
    }

    @Override
    public void update(Product object) {
        repository.put(object.getId(),object);
        saveToFile();
    }

    @Override
    public void delete(Product object) {
        repository.remove(object.getId());
        saveToFile();
    }

    @Override
    public Product getById(Long id) {
        return repository.get(id);
    }

    @Override
    public Product getByName(String name) {

        for(Product product: repository.values()) {
            if(product.getName().equals(name)) {
                return product;
            }
        }

        return null;
    }

    @Override
    public List<Product> getAll() {
        return new ArrayList<Product>(repository.values());
    }

    @Override
    public List<Product> getAllSortedByName() {

        List<Product> products = new ArrayList<>(repository.values());

        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product product1, Product product2) {
                String productName1 = product1.getName().toUpperCase();
                String productName2 = product2.getName().toUpperCase();

                //ascending order
                return productName1.compareTo(productName2);
            }
        });

        return products;
    }

    @Override
    public List<Product> getAllSortedByPrice() {
        
        List<Product> products = new ArrayList<>(repository.values());

        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product product1, Product product2) {

                //ascending order
                return Double.compare(product1.getPrice(), product2.getPrice());
            }
        });

        return products;
    }

    private static void saveToFile() {
        try (ObjectOutputStream stream =
                     new ObjectOutputStream(new FileOutputStream(fileName))) {
            stream.writeObject(repository);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readFromFile() {
        try (ObjectInputStream stream =
                     new ObjectInputStream(new FileInputStream(fileName))) {
            repository = (Map<Long, Product>) stream.readObject();
        }
        catch (ClassNotFoundException e) {
            System.out.println("Something wrong with objects in your file. Check the file content!");
            e.printStackTrace();
        }
        catch (FileNotFoundException e) {
            System.out.println("File with name ("+fileName+") was not found. Check the file!");
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
