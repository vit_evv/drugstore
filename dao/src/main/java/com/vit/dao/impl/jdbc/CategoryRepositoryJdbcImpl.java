package com.vit.dao.impl.jdbc;

import com.vit.dao.CategoryRepository;
import com.vit.dao.RepositoryProperties;
import com.vit.model.Category;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by evtushenkovv on 08.07.15.
 */
@Repository("categoryRepositoryJdbc")
public class CategoryRepositoryJdbcImpl implements CategoryRepository{

    private static RepositoryProperties repositoryProperties = RepositoryProperties.getInstance();

    static {
        try {
            Class.forName(repositoryProperties.getDriverClassName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void truncate() {

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             Statement ps = connection.createStatement()) {

            ps.execute("DROP TABLE IF EXISTS product");
            ps.execute("DROP TABLE IF EXISTS category");
            String sql = "CREATE TABLE IF NOT EXISTS `category` (" +
                    "  `id` bigint(20) NOT NULL AUTO_INCREMENT, " +
                    "  `name` varchar(50) NOT NULL, " +
                    "  PRIMARY KEY (`id`), " +
                    "  UNIQUE KEY `id_UNIQUE` (`id`), " +
                    "  UNIQUE KEY `name_UNIQUE` (`name`) " +
                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
            ps.execute(sql);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Long create(Category object) {

        String sql = "INSERT INTO category (name) VALUES (?)";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql
                     , Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, object.getName());

            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if(rs.next())
            {
                return rs.getLong(1);
            }
            return null;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Category object) {

        String sql = "UPDATE category " +
                "SET name = ? " +
                "WHERE id = ? ";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, object.getName());
            ps.setLong(2, object.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Category object) {

        String sql = "DELETE FROM category WHERE id = ?";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, object.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
    }

    @Override
    public Category getById(Long id) {

        Category category = null;
        String sql = "SELECT id, name FROM category WHERE id = ?";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                category = new Category(
                        rs.getLong("id"),
                        rs.getString("name"),
                        null
                );
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
        return category;
    }

    @Override
    public List<Category> getAll() {

        Category category = null;
        List<Category> categories = new ArrayList<>();
        String sql = "SELECT id, name FROM category";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                category = new Category(
                        rs.getLong("id"),
                        rs.getString("name"),
                        null
                );
                categories.add(category);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
        return categories;
    }

    @Override
    public List<Category> getAllSortedByAvgProductsPrice() {

        Category category = null;
        List<Category> categories = new ArrayList<>();
        String sql = "SELECT category.id, category.name, avg(price) AS avg_price " +
                "FROM category LEFT JOIN product " +
                "ON category.id = product.category_id " +
                "GROUP BY category.id, category.name " +
                "ORDER BY avg(price) DESC";

        try (Connection connection = DriverManager.getConnection(repositoryProperties.getUrl()
                , repositoryProperties.getUsername(), repositoryProperties.getPassword());
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                category = new Category(
                        rs.getLong("id"),
                        rs.getString("name"),
                        null
                );
                categories.add(category);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);

        }
        return categories;
    }

}
