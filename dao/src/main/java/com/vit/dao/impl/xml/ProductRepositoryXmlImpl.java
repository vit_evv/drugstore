package com.vit.dao.impl.xml;

import com.vit.dao.ProductRepository;
import com.vit.dao.RepositoryProperties;
import com.vit.model.Product;
import com.vit.model.dto.ProductsMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;


/**
 * Created by evtushenkovv on 24.06.15.
 */
public class ProductRepositoryXmlImpl implements ProductRepository {

    private static Long idCount = 0L;
    private static Map<Long, Product> repository = new HashMap<>();
    private static String fileName
            = RepositoryProperties.getInstance().getProductRepositoryPath();

    static {
        initializeRepository();
        idCount = (repository.size() == 0) ? 0 : repository.size()-1L;
    }

    @Override
    public Long create(Product object) {
        object.setId(idCount++);
        repository.put(object.getId(), object);
        saveToFile();
        return object.getId();
    }

    @Override
    public void update(Product object) {
        repository.put(object.getId(),object);
        saveToFile();
    }

    @Override
    public void delete(Product object) {
        repository.remove(object.getId());
        saveToFile();
    }

    @Override
    public Product getById(Long id) {
        return repository.get(id);
    }

    @Override
    public Product getByName(String name) {

        for(Product product: repository.values()) {
            if(product.getName().equals(name)) {
                return product;
            }
        }

        return null;
    }

    @Override
    public List<Product> getAll() {
        return new ArrayList<Product>(repository.values());
    }

    @Override
    public List<Product> getAllSortedByName() {

        List<Product> products = new ArrayList<>(repository.values());

        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product product1, Product product2) {
                String productName1 = product1.getName().toUpperCase();
                String productName2 = product2.getName().toUpperCase();

                //ascending order
                return productName1.compareTo(productName2);
            }
        });

        return products;
    }

    @Override
    public List<Product> getAllSortedByPrice() {
        
        List<Product> products = new ArrayList<>(repository.values());

        Collections.sort(products, new Comparator<Product>() {
            public int compare(Product product1, Product product2) {

                //ascending order
                return Double.compare(product1.getPrice(), product2.getPrice());
            }
        });

        return products;
    }

    private static void saveToFile() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ProductsMap.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(new ProductsMap(repository), new File(fileName));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private static void readFromFile() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ProductsMap.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            ProductsMap productsMap = (ProductsMap) unmarshaller.unmarshal(new File(fileName));
            repository = productsMap.getProducts();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private static void initializeRepository() {
        try {
            readFromFile();
        }
        catch (RuntimeException e) {
        }
    }
}
