package com.vit.dao.impl.xml;

import com.vit.dao.CategoryRepository;
import com.vit.dao.RepositoryProperties;
import com.vit.model.Category;
import com.vit.model.Product;
import com.vit.model.dto.CategoriesMap;
import com.vit.model.dto.ProductsMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;

/**
 * Created by evtushenkovv on 24.06.15.
 */
public class CategoryRepositoryXmlImpl implements CategoryRepository{

    private static Long idCount = 0L;
    private static Map<Long, Category> repository = new HashMap<>();
    private static String fileName
            = RepositoryProperties.getInstance().getCategoryRepositoryPath();

    static {
        initializeRepository();
        idCount = (repository.size() == 0) ? 0 : repository.size()-1L;
    }

    @Override
    public Long create(Category object) {
        object.setId(idCount++);
        repository.put(object.getId(), object);
        saveToFile();
        return object.getId();
    }

    @Override
    public void update(Category object) {
        repository.put(object.getId(),object);
        saveToFile();
    }

    @Override
    public void delete(Category object) {
        repository.remove(object.getId());
        saveToFile();
    }

    @Override
    public Category getById(Long id) {
        return repository.get(id);
    }

    @Override
    public List<Category> getAll() {
        return new ArrayList<Category>(repository.values());
    }

    @Override
    public List<Category> getAllSortedByAvgProductsPrice() {

        final List<Category> categories = new ArrayList<>(repository.values());

        Collections.sort(categories, new Comparator<Category>() {
            public int compare(Category category1, Category category2) {

                //descending order
                return Double.compare(getAvgPriceByCategory(category1), getAvgPriceByCategory(category2));
            }
        });

        return categories;
    }

    private Double getAvgPriceByCategory(Category category) {

        if (category.getProducts() == null) {
            return 0.0;
        }
        if (category.getProducts().size() == 0) {
            return 0.0;
        }

        Double sumPricesByCategory = 0.0;
        for(Product product: category.getProducts()) {
            sumPricesByCategory += (product.getPrice() == null)
                    ? 0 : product.getPrice();
        }

        return sumPricesByCategory/category.getProducts().size();
    }

    private static void saveToFile() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(CategoriesMap.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(new CategoriesMap(repository), new File(fileName));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void readFromFile() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(CategoriesMap.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            CategoriesMap categoriesMap = (CategoriesMap) unmarshaller.unmarshal(new File(fileName));
            repository = categoriesMap.getCategories();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private static void initializeRepository() {
        try {
            readFromFile();
        }
        catch (RuntimeException e) {
        }
    }
}
