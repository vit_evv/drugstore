package com.vit.dao;

import com.vit.model.Product;

import java.util.List;

/**
 * Created by evtushenkovv on 24.06.15.
 */
public interface ProductRepository {

    Long create(Product object);

    void update(Product object);

    void delete(Product object);

    Product getById(Long id);

    Product getByName(String name);

    List<Product> getAll();

    List<Product> getAllSortedByName();

    List<Product> getAllSortedByPrice();

}
