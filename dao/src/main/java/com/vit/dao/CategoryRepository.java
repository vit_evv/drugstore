package com.vit.dao;

import com.vit.model.Category;

import java.util.List;

/**
 * Created by evtushenkovv on 24.06.15.
 */
public interface CategoryRepository {

    Long create(Category object);

    void update(Category object);

    void delete(Category object);

    Category getById(Long id);

    List<Category> getAll();

    List<Category> getAllSortedByAvgProductsPrice();

}
