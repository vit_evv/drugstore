package com.vit.dao;

import com.vit.dao.factory.RepositoryFactory;
import com.vit.dao.factory.RepositoryFactoryProducer;
import com.vit.model.Category;
import com.vit.model.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.util.Calendar;

/**
 * Created by evtushenkovv on 25.06.15.
 */
public class MainRepository {

    private static final Logger LOGGER = LogManager.getLogger(MainRepository.class);

    public static void main(String[] args) {

        String repositoryType = RepositoryProperties.getInstance().getRepositoryType();
        RepositoryFactory repositoryFactory = RepositoryFactoryProducer.getFactory(repositoryType);
        ProductRepository productRepository = repositoryFactory.getProductRepository();
        CategoryRepository categoryRepository = repositoryFactory.getCategoryRepository();

        Calendar calendar = Calendar.getInstance();
        calendar.set(2016,6,23);

        LOGGER.debug(productRepository.getAll());

        Category category = new Category();
        category.setName("Spasmolytics");
        categoryRepository.create(category);
        category = new Category();
        category.setName("Awesome");
        category.setId(categoryRepository.create(category));

        Product product = new Product("Aspirine", "spasmolytic", "010235", 20.55, new Date(calendar.getTimeInMillis()));
        product.setCategory(category);
        productRepository.create(product);
        product = new Product("Some drug", "really useful", "000053", 13.01, new Date(calendar.getTimeInMillis()));
        product.setCategory(category);
        product.setId(productRepository.create(product));

        LOGGER.debug(categoryRepository.getAll());
        LOGGER.debug(categoryRepository.getById(1L));
        LOGGER.debug(categoryRepository.getAllSortedByAvgProductsPrice());

        LOGGER.debug(productRepository.getAll());
        LOGGER.debug(productRepository.getAllSortedByPrice());
        LOGGER.debug(productRepository.getAllSortedByName());
        LOGGER.debug(productRepository.getById(1L));
        LOGGER.debug(productRepository.getByName("Aspirine"));

//       Product product = new Product("Some drug", "really useful", "000053", 13.01, new Date(calendar.getTimeInMillis()));
//        product.setId(2L);
//        product.setDescription("it's amazing!");
//        productRepository.update(product);
//        LOGGER.debug(productRepository.getAll());

        category.setProducts(productRepository.getAll());
        categoryRepository.update(category);
        LOGGER.debug(categoryRepository.getAll());

        productRepository.delete(product);
        LOGGER.debug(productRepository.getAll());

        categoryRepository.delete(category);
        LOGGER.debug(categoryRepository.getAll());

    }

}
