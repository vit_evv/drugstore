package com.vit.dao.jdbc;

import com.vit.dao.CategoryRepository;
import com.vit.dao.factory.RepositoryFactory;
import com.vit.dao.factory.RepositoryFactoryProducer;
import com.vit.dao.impl.jdbc.CategoryRepositoryJdbcImpl;
import com.vit.model.Category;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * Created by evtushenkovv on 09.07.15.
 */
public class TestCategoryRepositoryJdbcImpl {

    private static final Logger LOGGER = LogManager.getLogger(TestCategoryRepositoryJdbcImpl.class);

    private static CategoryRepository repository;

    @BeforeClass
    public static void testsStarted() {

        LOGGER.debug("Testing started!");

        String repositoryType = "JDBC";
        RepositoryFactory repositoryFactory = RepositoryFactoryProducer.getFactory(repositoryType);
        repository = repositoryFactory.getCategoryRepository();

    }

    @AfterClass
    public static void testsFinished() {
        LOGGER.debug("Testing finished!");
    }

    @Before
    public void eachTestStarted() {
        CategoryRepositoryJdbcImpl.truncate();
    }

    @Test
    public void testCreateRead() {

        long expectedId = 1L;
        String expectedCategoryName = "Spasmolytics";
        Category expectedCategory = new Category(expectedCategoryName);

        long actualId = repository.create(expectedCategory);
        Category actualCategory = repository.getById(expectedId);
        String actualCategoryName = actualCategory.getName();

        assertEquals(expectedId, actualId);
        assertNotNull(actualCategory);
        assertEquals(expectedCategoryName, actualCategoryName);
    }


    @Test
    public void testUpdate() {

        long expectedId = 1L;
        Category expectedCategory = new Category("Spasmolytics");
        String expectedCategoryName = "New spasmolytics";

        expectedCategory.setId(repository.create(expectedCategory));
        expectedCategory.setName(expectedCategoryName);
        repository.update(expectedCategory);
        String actualCategoryName = repository.getById(expectedId).getName();

        assertEquals(expectedCategoryName, actualCategoryName);
    }

    @Test
    public void testDelete() {

        long expectedId = 1L;
        Category expectedCategory = new Category("Spasmolytics");

        expectedCategory.setId(repository.create(expectedCategory));
        repository.delete(expectedCategory);
        Category actualCategory = repository.getById(expectedId);

        assertNull(actualCategory);
    }

}
