package com.vit.dao.jdbc;

import com.vit.dao.ProductRepository;
import com.vit.dao.factory.RepositoryFactory;
import com.vit.dao.factory.RepositoryFactoryProducer;
import com.vit.dao.impl.jdbc.ProductRepositoryJdbcImpl;
import com.vit.model.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Date;
import java.util.Calendar;

import static org.junit.Assert.*;


/**
 * Created by evtushenkovv on 25.06.15.
 */
public class TestProductRepositoryJdbcImpl {

    private static final Logger LOGGER = LogManager.getLogger(TestProductRepositoryJdbcImpl.class);

    private static ProductRepository repository;
    private static Product product;

    @BeforeClass
    public static void testsStarted() {

        LOGGER.debug("Testing started!");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2015,Calendar.JULY,7,0,0,0);
        Date expirationDate = new Date(calendar.getTimeInMillis());
        product = new Product("Aspirine", "spasmolytic", "010235", 20.55, expirationDate);

        String repositoryType = "JDBC";
        RepositoryFactory repositoryFactory = RepositoryFactoryProducer.getFactory(repositoryType);
        repository = repositoryFactory.getProductRepository();

    }

    @AfterClass
    public static void testsFinished() {
        LOGGER.debug("Testing finished!");
    }

    @Before
    public void eachTestStarted() {
        ProductRepositoryJdbcImpl.truncate();
    }

    @Test
    public void testCreateRead() {

        long expectedId = 1L;

        long actualId = repository.create(product);
        Product actualProduct = repository.getById(expectedId);
        boolean actualEquality = product.equalsByAllFieldsWithoutId(actualProduct);

        assertEquals(expectedId, actualId);
        assertTrue(actualEquality);

    }

    @Test
    public void testUpdate() {

        long expectedId = 1L;
        String expectedProductDescription = "it's amazing!";

        product.setId(repository.create(product));
        product.setDescription(expectedProductDescription);
        repository.update(product);
        String actualProductDescription = repository.getById(expectedId).getDescription();

        assertEquals(expectedProductDescription, actualProductDescription);

    }

    @Test
    public void testDelete() {

        long expectedId = 1L;

        product.setId(repository.create(product));
        repository.delete(product);
        Product actualProduct = repository.getById(expectedId);

        assertNull(actualProduct);

    }

}
