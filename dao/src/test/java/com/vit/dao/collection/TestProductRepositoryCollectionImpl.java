package com.vit.dao.collection;

import com.vit.dao.ProductRepository;
import com.vit.dao.factory.RepositoryFactory;
import com.vit.dao.factory.RepositoryFactoryProducer;
import com.vit.dao.impl.collections.ProductRepositoryCollectionImpl;
import com.vit.model.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Calendar;
import java.sql.Date;

import static org.junit.Assert.*;


/**
 * Created by evtushenkovv on 25.06.15.
 */
public class TestProductRepositoryCollectionImpl {

    private static final Logger LOGGER = LogManager.getLogger(TestProductRepositoryCollectionImpl.class);

    private static Calendar calendar;
    private static Long id;
    private static String name;
    private static String description;
    private static String sku;
    private static Double price;
    private static Date expirationDate;
    private static Product product;
    private static ProductRepository repository;

    @BeforeClass
    public static void testsStarted() {

        LOGGER.debug("Testing started!");

        id = 0L;
        name = "Aspirine";
        description = "spasmolytic";
        sku = "010235";
        price = 20.55;
        calendar = Calendar.getInstance();
        expirationDate = new Date(calendar.getTimeInMillis());

        product = new Product(name, description, sku, price, expirationDate);

        String repositoryType = "COLLECTION";
        RepositoryFactory repositoryFactory = RepositoryFactoryProducer.getFactory(repositoryType);
        repository = repositoryFactory.getProductRepository();

    }

    @AfterClass
    public static void testsFinished() {
        LOGGER.debug("Testing finished!");
    }

    @After
    public void eachTestFinished() {
        ProductRepositoryCollectionImpl.truncate();
    }

    @Test
    public void testCreateRead() {

        long expectedId = 1L;

        long actualId = repository.create(product);
        Product actualProduct = repository.getById(expectedId);
        boolean actualEquality = product.equalsByAllFieldsWithoutId(actualProduct);

        assertEquals(expectedId, actualId);
        assertTrue(actualEquality);

    }

    @Test
    public void testUpdate() {

        long expectedId = 1L;
        String expectedProductDescription = "it's amazing!";

        product.setId(repository.create(product));
        product.setDescription(expectedProductDescription);
        repository.update(product);
        String actualProductDescription = repository.getById(expectedId).getDescription();

        assertEquals(expectedProductDescription, actualProductDescription);

    }

    @Test
    public void testDelete() {

        long expectedId = 1L;

        product.setId(repository.create(product));
        repository.delete(product);
        Product actualProduct = repository.getById(expectedId);

        assertNull(actualProduct);

    }

}
