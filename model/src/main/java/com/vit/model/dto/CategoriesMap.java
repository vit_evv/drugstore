package com.vit.model.dto;

import com.vit.model.Category;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by evtushenkovv on 27.06.15.
 */
@XmlRootElement(name = "categories")
@XmlAccessorType(XmlAccessType.FIELD)
public class CategoriesMap {

    private Map<Long, Category> categoryMap = new HashMap<>();

    public CategoriesMap() {
    }

    public CategoriesMap(Map<Long, Category> categories) {
        this.categoryMap = categories;
    }

    public Map<Long, Category> getCategories() {
        return categoryMap;
    }

    public void setCategories(Map<Long, Category> categories) {
        this.categoryMap = categories;
    }
}
