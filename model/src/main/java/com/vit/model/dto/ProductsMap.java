package com.vit.model.dto;

import com.vit.model.Product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by evtushenkovv on 27.06.15.
 */
@XmlRootElement(name = "products")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductsMap {

    private Map<Long, Product> productMap = new HashMap<>();

    public ProductsMap() {
    }

    public ProductsMap(Map<Long, Product> products) {
        this.productMap = products;
    }

    public Map<Long, Product> getProducts() {
        return productMap;
    }

    public void setProducts(Map<Long, Product> products) {
        this.productMap = products;
    }
}
