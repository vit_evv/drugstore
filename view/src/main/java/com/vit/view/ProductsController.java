package com.vit.view;

import com.vit.model.Product;
import com.vit.service.ProductService;
import com.vit.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Vitalii Ievtushenko on 19.07.2015 11:19.
 */
@Controller
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String  getProductsList(Model model){

        List<Product> products = productService.getAll();
        model.addAttribute("products", products);

        return "products.page";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> create(@RequestParam String name,
                                                @RequestParam String sku,
                                                @RequestParam String price,
                                                @RequestParam String expirationDate,
                                                @RequestParam String description,
                                                @RequestParam String categoryName){

        Product product = new Product(name,description,sku,Double.parseDouble(price),null);
        Long id = productService.create(product);

        return new ResponseEntity<String>(id.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> update(@RequestParam Long id,
                                         @RequestParam String name,
                                         @RequestParam String sku,
                                         @RequestParam Double price,
                                         @RequestParam String expirationDate,
                                         @RequestParam String description,
                                         @RequestParam String categoryName){

        Product product = new Product(name, description, sku, price, null);
        product.setId(id);
        productService.update(product);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> delete(@PathVariable("id") Long id){

        Product product = new Product();
        product.setId(id);
        productService.delete(product);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

}
