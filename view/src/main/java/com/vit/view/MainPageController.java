package com.vit.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Vitalii Ievtushenko on 18.07.2015 19:25.
 */
@Controller
public class MainPageController {

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String  defaultPage(){
        return "index.page";
    }

}
