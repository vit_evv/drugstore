package com.vit.view;

import com.vit.model.Category;
import com.vit.service.CategoryService;
import com.vit.service.impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Vitalii Ievtushenko on 19.07.2015 11:19.
 */
@Controller
@RequestMapping("/categories")
public class CategoriesController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String  getCategoriesList(Model model){

        List<Category> categories = categoryService.getAll();
        model.addAttribute("categories",categories);

        return "categories.page";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> create(@RequestParam String name){

        Category category = new Category(name);
        Long id = categoryService.create(category);

        return new ResponseEntity<String>(id.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<String> update(@RequestParam Long id,
                                         @RequestParam String name){

        Category category = new Category(name);
        category.setId(id);
        categoryService.update(category);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<String> delete(@PathVariable("id") Long id){

        Category category = new Category();
        category.setId(id);
        categoryService.delete(category);

        return new ResponseEntity<String>(HttpStatus.OK);
    }

}
