/**
 * Created by vit on 19.07.2015.
 */
$(document).ready(function () {

    $('#productsTable').DataTable();

    $('#addNewProductButton').on('click', function(event){
        event.preventDefault();
        createProduct();
    });

    $('.updateProductButton').on('click', function(event){
        event.preventDefault();
        update();
    });

    $('.deleteProductButton').on('click', function(event){
        event.preventDefault();
        deleteObj(event.currentTarget);
    });

});

function createProduct(){

     $.ajax({
        type:'POST',
        url:"/products"+
                        "?name="+$('#productName').val()+
                        "&sku="+$('#productSku').val()+
                        "&price="+$('#productPrice').val()+
                        "&description="+$('#productDescription').val()+
                        "&expirationDate="+$('#productExpirationDate').val()+
                        "&categoryName="+$('#productCategoryName').val(),
        dataType: "text",
        success: function(data){
            window.location="/products";
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert(textStatus);
        }
    });
}


function update(){

}

function deleteObj(currentButton){

    var objectId = $(currentButton).closest('tr').children('td:eq(0)').text();

    $(".dialog-confirm").dialog({
        resizable: false,
        height:200,
        minWith:600,
        modal: true,
        buttons: {
            "Delete": function() {
                callDeleteObj(objectId);
                $(this).dialog("close");
            },
            Cancel: function() {
                $(this).dialog("close");
            }
        }
    });
}

function callDeleteObj(objectId){

    $.ajax({
        type:'DELETE',
        url:"/products/"+objectId,
        dataType: "text",
        success: function(data){
            window.location="/products";
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert(textStatus);
        }
    });
}