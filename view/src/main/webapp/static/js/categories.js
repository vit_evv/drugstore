/**
 * Created by vit on 19.07.2015.
 */
$(document).ready(function () {

    $('#categoriesTable').DataTable();

    $('#addNewCategoryButton').on( 'click',function(event){
        event.preventDefault();
        create();
    });

    $('.updateCategoryButton').on( 'click',function(event){
        event.preventDefault();
        update();
    });

    $('.deleteCategoryButton').on( 'click',function(event){
        event.preventDefault();
        deleteObj(event.currentTarget);
    });

});

function create(){

    $.ajax({
        type:'POST',
        url:"/categories?name="+ $('#categoryName').val(),
        dataType: "text",
        success: function(data){
            window.location="/categories";
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert(textStatus);
        }
    });
}

function update(){

//    $.ajax({
//        type:'PUT',
//        url:"/categories" +
//            "?id=" +
//            "name="+ $('#categoryName').val(),
//        dataType: "text",
//        success: function(data){
//            window.location="/categories";
//        },
//        error: function (jqXHR, textStatus, errorThrown){
//            alert(textStatus);
//        }
//    });
}

function deleteObj(currentButton){

    var objectId = $(currentButton).closest('tr').children('td:eq(0)').text();

    $(".dialog-confirm").dialog({
        resizable: false,
        height:200,
        with:600,
        modal: true,
        buttons: {
            "Delete": function() {
                callDeleteObj(objectId);
                $(this).dialog("close");
            },
            Cancel: function() {
                $(this).dialog("close");
            }
        }
    });
}

function callDeleteObj(objectId){

    $.ajax({
        type:'DELETE',
        url:"/categories/"+objectId,
        dataType: "text",
        success: function(data){
            window.location="/categories";
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert(textStatus);
        }
    });
}
