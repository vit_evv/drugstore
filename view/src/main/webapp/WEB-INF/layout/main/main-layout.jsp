<%--
 Created by Vitalii Ievtushenko on 18.07.2015 19:33.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<html>
<head>
  <title><tiles:getAsString name="main.title"></tiles:getAsString></title>

  <link rel="stylesheet" href="../static/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="../static/css/bootstrap-theme.min.css"/>
  <link rel="stylesheet" href="../static/css/main.css"/>

</head>

<body>

    <div id="header">
      <tiles:insertAttribute name="main.header"></tiles:insertAttribute>
    </div>

    <div id="content">
      <tiles:insertAttribute name="main.content"></tiles:insertAttribute>
    </div>

    <footer>
      <tiles:insertAttribute name="main.footer"></tiles:insertAttribute>
    </footer>

</body>

</html>
