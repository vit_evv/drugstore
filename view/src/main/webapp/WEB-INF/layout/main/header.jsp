<%--
 Created by Vitalii Ievtushenko on 18.07.2015 19:31.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="navbar navbar-default navbar-fixed-top">

    <div class="container-fluid">
        <div>
            <ul class="nav navbar-nav navbar-right-center">
                <li><a href="/">Home</a></li>
                <li><a href="/categories">Categories</a></li>
                <li><a href="/products">Products</a></li>
            </ul>
        </div>
    </div>

</nav>
