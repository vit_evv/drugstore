<%--
  Created by IntelliJ IDEA.
  User: evtushenkovv
  Date: 19.07.2015
  Time: 11:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/plug-ins/1.10.7/integration/jqueryui/dataTables.jqueryui.css">

    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="http://cdn.datatables.net/plug-ins/1.10.7/integration/jqueryui/dataTables.jqueryui.js"></script>
    <script src="../static/js/categories.js"></script>

</head>
<body>

<div>
    <table id="categoriesTable" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td><strong>Id</strong></td>
            <td><strong>Name</strong></td>
            <td>Update</td>
            <td>Delete</td>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td></td>
            <td><input type="text" id="categoryName" width="100%"></td>
            <td><button id="addNewCategoryButton">Add</button></td>
            <td></td>
        </tr>
        <tr>
            <td><strong>Id</strong></td>
            <td><strong>Name</strong></td>
            <td>Update</td>
            <td>Delete</td>
        </tr>
        <tfoot>
        <tbody>
        <c:forEach var="category" items="${categories}">
            <tr>
                <td><c:out value="${category.id}"/></td>
                <td><c:out value="${category.name}"/></td>
                <td><button class="updateCategoryButton">Upd</button></td>
                <td><button class="deleteCategoryButton">Del</button></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="dialog-confirm" title="Delete this item?">
    <p>
        <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
        This item will be permanently deleted and cannot be recovered. Are you sure?
    </p>
</div>

</body>
</html>
