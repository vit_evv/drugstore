package com.vit.service;

import com.vit.model.Product;

import java.util.List;

/**
 * Created by Vitalii Ievtushenko on 18.07.2015 17:31.
 */
public interface ProductService {

    Long create(Product object);

    void update(Product object);

    void delete(Product object);

    Product getById(Long id);

    Product getByName(String name);

    List<Product> getAll();

    List<Product> getAllSortedByName();

    List<Product> getAllSortedByPrice();
}
