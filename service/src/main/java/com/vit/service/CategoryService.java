package com.vit.service;

import com.vit.model.Category;

import java.util.List;

/**
 * Created by Vitalii Ievtushenko on 18.07.2015 17:32.
 */
public interface CategoryService {

    Long create(Category object);

    void update(Category object);

    void delete(Category object);

    Category getById(Long id);

    List<Category> getAll();

    List<Category> getAllSortedByAvgProductsPrice();
}
