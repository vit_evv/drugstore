package com.vit.service.impl;

import com.vit.dao.ProductRepository;
import com.vit.dao.impl.collections.ProductRepositoryCollectionImpl;
import com.vit.dao.impl.jdbc.ProductRepositoryJdbcImpl;
import com.vit.model.Product;
import com.vit.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vitalii Ievtushenko on 18.07.2015 17:47.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    @Qualifier("productRepositoryJdbc")
    ProductRepository productRepository;

    @Override
    public Long create(Product object) {
        return productRepository.create(object);
    }

    @Override
    public void update(Product object) {
        productRepository.update(object);
    }

    @Override
    public void delete(Product object) {
        productRepository.delete(object);
    }

    @Override
    public Product getById(Long id) {
        return productRepository.getById(id);
    }

    @Override
    public Product getByName(String name) {
        return productRepository.getByName(name);
    }

    @Override
    public List<Product> getAll() {
        return productRepository.getAll();
    }

    @Override
    public List<Product> getAllSortedByName() {
        return productRepository.getAllSortedByName();
    }

    @Override
    public List<Product> getAllSortedByPrice() {
        return productRepository.getAllSortedByPrice();
    }
}
