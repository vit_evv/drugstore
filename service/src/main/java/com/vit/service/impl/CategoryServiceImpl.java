package com.vit.service.impl;

import com.vit.dao.CategoryRepository;
import com.vit.dao.impl.collections.CategoryRepositoryCollectionImpl;
import com.vit.dao.impl.jdbc.CategoryRepositoryJdbcImpl;
import com.vit.model.Category;
import com.vit.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vitalii Ievtushenko on 18.07.2015 17:46.
 */
@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    @Qualifier("categoryRepositoryJdbc")
    CategoryRepository categoryRepository;

    @Override
    public Long create(Category object) {
        return categoryRepository.create(object);
    }

    @Override
    public void update(Category object) {
        categoryRepository.update(object);
    }

    @Override
    public void delete(Category object) {
        categoryRepository.delete(object);
    }

    @Override
    public Category getById(Long id) {
        return categoryRepository.getById(id);
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public List<Category> getAllSortedByAvgProductsPrice() {
        return categoryRepository.getAllSortedByAvgProductsPrice();
    }
}
