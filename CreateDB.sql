
CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `product` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `description` TEXT NULL,
  `sku` VARCHAR(16) NOT NULL,
  `price` NUMERIC(9,2) ZEROFILL NULL DEFAULT 0.000,
  `expiration_date` TIMESTAMP NULL,
  `category_id` BIGINT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `sku_UNIQUE` (`sku` ASC),
  CONSTRAINT `category_id`
  FOREIGN KEY (`category_id`)
  REFERENCES `category` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
  ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `category`
(`name`) VALUES ("Spasmolytics");
INSERT INTO `category`
(`name`) VALUES ("Allergenics");
INSERT INTO `category`
(`name`) VALUES ("Antacids");
INSERT INTO `category`
(`name`) VALUES ("Antiviral agents");
INSERT INTO `category`
(`name`) VALUES ("Antidepressants");

INSERT INTO `product`
(`name`,`description`,`sku`,`price`,`expiration_date`,`category_id`)
VALUES("Aspirine","Not strong spasmolytic","010235",20.55,'2016-07-07',1);
INSERT INTO `product`
(`name`,`description`,`sku`,`price`,`expiration_date`,`category_id`)
VALUES("Tramadol","Very Strong spasmolytic","010236",99.99,'2019-07-07',1);
INSERT INTO `product`
(`name`,`description`,`sku`,`price`,`expiration_date`,`category_id`)
VALUES("Grastek","Timothy grass pollen allergen extract","010896",11.05,'2017-10-07',2);
INSERT INTO `product`
(`name`,`description`,`sku`,`price`,`expiration_date`,`category_id`)
VALUES("Oralair","Mixed grass pollens allergen extract","011045",200.00,'2015-12-31',2);
INSERT INTO `product`
(`name`,`description`,`sku`,`price`,`expiration_date`,`category_id`)
VALUES("Wellbutrin","Used to treat major depressive disorder","010200",356.01,'2019-01-01',5);
